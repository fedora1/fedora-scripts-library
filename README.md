# Great Library of scripts for Fedora RPM package maintainers



## What it is:

A lot of package maintainers in Fedora has alot of custom scripts, tools, tips and tricks.

Let's share them amongst us to enhance the collective package maintainer experience !

## How this works:

This project is supposed to be a web service, where anyone can post it's knowledge and scripts,
and anyone can browse it and use it.

## Roadmap:

1/ create a web service in Python 3 and Flask

2/ add MariaDB database backend

3/ create a complete 'venv' for the service

4/ create a container to easily deploy the whole project

## Contributing

Ask me.

## Project status

Early development - trying to create a proof of concept
